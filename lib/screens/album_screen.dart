import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sample_app/bloc/album/album_bloc.dart';
import 'package:sample_app/model/album.dart';
import 'package:sample_app/widgets/list_row.dart';

class AlbumScreen extends StatefulWidget {
  @override
  _AlbumScreenState createState() => _AlbumScreenState();
}

class _AlbumScreenState extends State<AlbumScreen> {
  @override
  void initState() {
    _loadAlbums();
    super.initState();
  }

  _loadAlbums() async {
    BlocProvider.of<AlbumBloc>(context).add(AlbumEvent.fetchAlbums);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Sample App for Testing"),
        ),
        body: Center(
          child: _body(),
        ));
  }

  _body() {
    return Column(
      children: [
        BlocBuilder<AlbumBloc, AlbumState>(
          builder: (BuildContext context, AlbumState state) {
            if (state is AlbumListError) {
              return Text("error happened");
            }
            if (state is AlbumLoaded) {
              return _list(state.albums);
            }

            return CircularProgressIndicator();
          },
        )
      ],
    );
  }

  Widget _list(List<Album> albums) {
    return Expanded(
        child: ListView.builder(
      itemCount: albums.length,
      itemBuilder: (_, index) {
        Album album = albums[index];
        return ListRow(
          album: album,
        );
      },
    ));
  }
}
