import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:sample_app/api/services.dart';
import 'package:sample_app/model/album.dart';

part 'album_event.dart';
part 'album_state.dart';

class AlbumBloc extends Bloc<AlbumEvent, AlbumState> {
  final AlbumsRepo albumsRepo;
  List<Album> albumList;

  AlbumBloc({this.albumsRepo}) : super(AlbumInitial());

  @override
  Stream<AlbumState> mapEventToState(
    AlbumEvent event,
  ) async* {
    switch (event) {
      case AlbumEvent.fetchAlbums:
        yield AlbumLoading();
        try {
          albumList = await albumsRepo.getAlbumList();
          yield AlbumLoaded(albums: albumList);
        } catch (e) {
          yield AlbumListError(error: "something Wrong");
        }

        break;
      default:
    }
  }
}
