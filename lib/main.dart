import 'package:flutter/material.dart';
import 'package:sample_app/api/services.dart';
import 'package:sample_app/bloc/album/album_bloc.dart';
import 'screens/album_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: BlocProvider(
          create: (context) => AlbumBloc(albumsRepo: AlbumServices()),
          child: AlbumScreen(),
        ));
  }
}
